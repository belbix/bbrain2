package pro.belbix.bbrain2.neuron;

import pro.belbix.bbrain2.exceptions.WrongNeuronState;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NeuronUtils {

    public static <T> void clearOld(Map<Long, T> values, long countForLife, long iterate) {
        List<Long> keysForRemove = new ArrayList<>();
        for (Long key : values.keySet()) {
            long lifeTime = iterate - key + 1;
            if (iterate < key)
                throw new WrongNeuronState("Trying to clear future value, iterate("
                        + iterate + ") < key(" + key + ")");
            if (lifeTime > countForLife) {
                keysForRemove.add(key);
            }
        }
        for (Long key : keysForRemove) {
            values.remove(key);
        }
    }

    public static float calcDynamicValues(Map<Long, Float> map, long countForLife, long iterate) {
        float v = 0;
        for (Long key : map.keySet()) {
            if (iterate < key)
                throw new WrongNeuronState("Trying to calculate past value, iterate("
                        + iterate + ") < key(" + key + ")");
            double lifeTime = (iterate - key);
            if (lifeTime > countForLife) continue;
            double k = 1;
            if (lifeTime > 0) k = (lifeTime / (double) countForLife);
            Float f = map.get(key);
            if(f == null) continue;
            v += f * k;
        }
        return v;
    }

}
