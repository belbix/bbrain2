package pro.belbix.bbrain2.neuron;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class NeuronVisualState {

    private double lastActualValues;
    private double lastValues;
    private Instant lastValueTime;
    private boolean lastActivate;
    private Instant lastActivateTime;
    private long countWithoutActivity = 0;
    float x;
    float y;
    float z;

}
