package pro.belbix.bbrain2.neuron;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class NeuronConfig {
    boolean printValues = false; // for test
    private Map<NeuronKey, Neuron> allNeurons;

    //--------------- FOR NEURON --------------------
    private boolean educateZeroLayer = true;
    private boolean zeroLayerPowerSimple = false;
    private boolean mutateThresholdEnable = true;

    private long countForLife = 1;
    private long countForLifeDecisions = 100;
    private float maxEducateFactor = 1000;

    private float maxThreshold = 10;
    private float minThreshold = 0;
    private float thresholdMutate = 0.01f;

    private float maxPower = 10;
    private float minPower = -10;
    private float powerMutate = 0.01f;

    private float mutateFactorMax = 10;
    private float mutateFactorMin = 0.1f;
    private float mutateFactorMutate = 0.1f;
    //--------------------------------------------------

    //----------------FOR CREATING--------------------
    private boolean useInhibitor = true;
    private boolean useReverse = true;
    private boolean useFinisher = true; //opposite for initiator
    private long radiusForConnect = 300;
    //--------------------------------------------------

    public void simplePerceptron(){
        educateZeroLayer = false;
        zeroLayerPowerSimple = true;
        mutateThresholdEnable = false;
    }

}
