package pro.belbix.bbrain2.neuron;

import pro.belbix.bbrain2.exceptions.NeuronNotFound;

import java.time.Instant;

public class NeuronCalculator {


    private static void activateNeuron(Neuron neuron) {
        if (neuron.isActivate()) return;
        if (!neuron.compareValue()) return;
        neuron.setActivate(true);
        if (neuron.getNeuronVisualState() != null) {
            neuron.getNeuronVisualState().setLastActivate(true);
            neuron.getNeuronVisualState().setLastActivateTime(Instant.now());
            neuron.getNeuronVisualState().setCountWithoutActivity(0);
        }

        send(neuron, null);

        neuron.clearAfterActivate();
    }

    public static void activateNeuron(Neuron neuron, Boolean positive) {
        if (positive == null) {
            activateNeuron(neuron);
            return;
        }
        if (!neuron.compareValue()) return;
        if (positive && neuron.isSendPositive()) return;
        if (!positive && neuron.isSendNegative()) return;

        send(neuron, positive);
        if (positive) {
            neuron.setSendPositive(true);
        } else {
            neuron.setSendNegative(true);
        }
        if (neuron.isSendNegative() && neuron.isSendPositive()) {
            neuron.setActivate(true);
            neuron.clearAfterActivate();
        }
    }

    private static void send(Neuron neuron, Boolean positive) {
        if (neuron.getNextNeurons() == null || neuron.getNextNeurons().isEmpty()) return;

        for (NeuronKey key : neuron.getNextNeurons()) {
            Neuron nextNeuron = neuron.getNeuronConfig().getAllNeurons().get(key);
            Float power = nextNeuron.getSynapses().get(neuron.getKey());
            if (power == null) throw new NeuronNotFound("Power not found for " + neuron.getKey());
            if (positive == null) {
                if (power == 0) continue;
            } else if (positive) {
                if (power <= 0) continue;
            } else {
                if (power >= 0) continue;
            }
            nextNeuron.plusValue(power);
        }
    }

}
