package pro.belbix.bbrain2.neuron;

import lombok.Getter;
import lombok.Setter;
import pro.belbix.bbrain2.exceptions.NeuronNotFound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Network {
    private NeuronConfig neuronConfig;
    private List<Map<Long, Neuron>> receptorLayers;
    private List<Map<Long, Neuron>> hiddenLayers;
    private List<Neuron> results;

    public Map<Long, Neuron> getHiddenLayerByKey(NeuronKey key) {
        for (Map<Long, Neuron> layer : hiddenLayers) {
            NeuronKey hKey = layer.values().iterator().next().getKey();

            if (
                    hKey.isInhibitor() == key.isInhibitor()
                            && hKey.isInitiator() == key.isInitiator()
                            && hKey.isReverse() == key.isReverse()
            ) {
                return layer;
            }
        }
        throw new NeuronNotFound("Not found layer for " + key);
    }

    public List<Map<Long, Neuron>> getHiddenLayersByKey(NeuronKey key, long layerId) {
        List<Map<Long, Neuron>> layers = new ArrayList<>();
        for (Map<Long, Neuron> layer : hiddenLayers) {
            NeuronKey hKey = layer.values().iterator().next().getKey();

            if (hKey.isInitiator() == key.isInitiator()
                    && hKey.isReverse() == key.isReverse()
                    && hKey.getLayer() == layerId
            ) {
                layers.add(layer);
            }
        }
        if (layers.isEmpty()) throw new NeuronNotFound("Not found layer for " + key);
        return layers;
    }

    public Neuron getResultLayerByKey(NeuronKey key) {
        for (Neuron neuron : results) {
            NeuronKey rKey = neuron.getKey();

            if (
                    rKey.isInitiator() == key.isInitiator()
                    && rKey.isReverse() == key.isReverse()
            ) {
                return neuron;
            }
        }
        throw new NeuronNotFound("Not found layer for " + key);
    }


    public boolean result(NeuronKey key) {
        for (Neuron neuron : results) {
            NeuronKey rKey = neuron.getKey();
            if (
                    rKey.getId() != key.getId()
                            || rKey.isReverse() != key.isReverse()
                            || rKey.isInitiator() != key.isInitiator()
            ) continue;
            return neuron.isActivate();
        }
        throw new NeuronNotFound("Result not found for key: " + key);
    }

    public Map<Long, Boolean> results(NeuronKey key) {
        Map<Long, Boolean> resultsMap = new HashMap<>();
        for (Neuron neuron : results) {
            NeuronKey rKey = neuron.getKey();
            if (rKey.isReverse() != key.isReverse()
                    || rKey.isInitiator() != key.isInitiator()
            ) continue;
            resultsMap.put(rKey.getId(), neuron.isActivate());
        }
        if (resultsMap.isEmpty())
            throw new NeuronNotFound("Result not found for key: " + key);
        return resultsMap;
    }
}
