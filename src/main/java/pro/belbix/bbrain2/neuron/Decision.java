package pro.belbix.bbrain2.neuron;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Decision {
    private long iterate;
    private boolean activate;
}
