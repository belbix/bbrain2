package pro.belbix.bbrain2.neuron;

import java.util.Map;

public class Printer {

    public static String printNetwork(Network network) {
        String msg = "";
        msg += "-------RECEPTORS--------\n";
        for (Map<Long, Neuron> rl : network.getReceptorLayers()) {
            for (Neuron neuron : rl.values()) {
                msg += printNeuron(neuron);
            }
        }

        msg += "-------HIDDEN----------\n";

        for (Map<Long, Neuron> rl : network.getHiddenLayers()) {
            for (Neuron neuron : rl.values()) {
                msg += printNeuron(neuron);
            }
        }

        msg += "-------RESULT----------\n";
        for (Neuron neuron : network.getResults()) {
            msg += printNeuron(neuron);
        }
        return msg;
    }

    private static String printNeuron(Neuron neuron) {
        String msg = "";
        msg += "name:" + neuron.getKey() + "\n";
        msg += "thrld:" + neuron.getThreshold() + "\n";
        for (NeuronKey key : neuron.getSynapses().keySet()) {
            msg += key + " " + neuron.getSynapses().get(key) + "; ";
        }
        msg += "\n";
        return msg;
    }

}
