package pro.belbix.bbrain2.neuron;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import pro.belbix.bbrain2.Utils;
import pro.belbix.bbrain2.exceptions.DecisionNotFound;
import pro.belbix.bbrain2.exceptions.NeuronException;
import pro.belbix.bbrain2.exceptions.WrongNeuronState;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Getter
@Setter
public class Neuron implements BasicNeuron {

    //--------------- VARS FOR SAVE ---------------------
    private final NeuronKey key;
    private final boolean inhibitor;
    private final boolean reverse; //reverse neuron activate when signal decrease
    private final boolean initiator;

    private float mutateFactor = 10;
    float threshold = Utils.random();
    private List<NeuronKey> nextNeurons = new ArrayList<>();
    private Map<NeuronKey, Float> synapses = new TreeMap<>();
    //-----------------------------------------------------

    // ---------- RUNTIME VARIABLES -----------------
    @JsonIgnore
    private Map<Long, Float> values = new ConcurrentSkipListMap<>();
    @JsonIgnore
    private boolean activate = false;
    @JsonIgnore
    private boolean sendPositive = false;
    @JsonIgnore
    private boolean sendNegative = false;
    @JsonIgnore
    private TreeMap<Long, Decision> decisions = new TreeMap<>();
    @JsonIgnore
    private long iterateCount = 0;

    @JsonIgnore
    private NeuronConfig neuronConfig;
    @JsonIgnore
    private NeuronVisualState neuronVisualState;

    public Neuron(long id, long layer, boolean inhibitor, boolean reverse, boolean initiator) {
        this.key = new NeuronKey(id, layer, inhibitor, reverse, initiator);
        this.inhibitor = inhibitor;
        this.reverse = reverse;
        this.initiator = initiator;
    }

    public void plusValue(float value) {
        values.merge(iterateCount, value, (a, b) -> b + a);
    }

    void clearAfterActivate() {
        values.clear();
    }

    public void iterateFinish() {
        iterateCount++;
        NeuronUtils.clearOld(values, neuronConfig.getCountForLife(), iterateCount);
        NeuronUtils.clearOld(decisions, neuronConfig.getCountForLifeDecisions(), iterateCount);
        activate = false;
        sendPositive = false;
        sendNegative = false;
    }

    public void clear(){
        values.clear();
        activate = false;
        sendPositive = false;
        sendNegative = false;
        decisions.clear();
        iterateCount = 0;
    }

    private float actualValue() {
        float v = NeuronUtils.calcDynamicValues(values, neuronConfig.getCountForLife(), iterateCount);

        if (neuronConfig.isPrintValues()) {
            StringBuilder msg = new StringBuilder(key + " - " + iterateCount + " values - ");
            for (Long keyIterate : values.keySet()) {
                msg.append(keyIterate).append(":").append(values.get(keyIterate)).append(";");
            }
            System.out.println(msg);
        }

        if (neuronVisualState != null) {
            neuronVisualState.setLastValues(v);
            neuronVisualState.setLastValueTime(Instant.now());
            neuronVisualState.setLastActualValues(v);
        }
        return v;
    }

    boolean compareValue() {
        float v = actualValue();
        if (
                (initiator && !reverse && !inhibitor) //SRSI_OPEN long
                        || (!initiator && reverse && !inhibitor) //SRSI_CLOSE short
                        || (initiator && reverse && inhibitor) //SRSI_OPEN_I short
                        || (!initiator && !reverse && inhibitor) //SRSI_CLOSE_I long
        ) {
            return v >= threshold;
        }

        //SRSI_OPEN short
        //SRSI_CLOSE long
        //SRSI_OPEN_I long
        return v < threshold;

    }

    public void educateNegative(float educateFactor, boolean needToReact, long iterate) {
        if (key.getLayer() == 0 && !neuronConfig.isEducateZeroLayer()) return;

        boolean wasActive = wasActive(iterate);
        if (educateFactor > neuronConfig.getMaxEducateFactor()) educateFactor = neuronConfig.getMaxEducateFactor();
        modifyMutateFactorNegative(wasActive);
        if (needToReact) {
            modifyForReact(educateFactor, wasActive, iterate);
        } else {
            modifyForNoReact(educateFactor, wasActive, iterate);
        }
    }

    public void educatePositive(float educateFactor, long iterate) {
        if (key.getLayer() == 0 && !neuronConfig.isEducateZeroLayer()) return;

        boolean wasActive = wasActive(iterate);
        modifyMutateFactorPositive(wasActive);
//        modifyForReact(educateFactor, wasActive, iterate);
    }

    public void educateLazy(float educateFactor, long iterate) {
        if (key.getLayer() == 0 && !neuronConfig.isEducateZeroLayer()) return;

        boolean wasActive = wasActive(iterate);
        if (!wasActive) {
            makeMoreActiveThreshold(educateFactor);
        }
        powersForReact(educateFactor, iterate);
    }

    private boolean wasActive(long iterate) {
        if (iterate == 0) return false;
        Decision decision = decisions.get(iterate);
        if (decision == null)
            throw new DecisionNotFound("Decision " + iterate + " not found for neuron " + key);
        return decision.isActivate();
    }


    private void modifyMutateFactorNegative(boolean wasActive) {
        if (!wasActive) return;
        mutateFactor += neuronConfig.getMutateFactorMutate() * Utils.random();
        if (mutateFactor > neuronConfig.getMutateFactorMax()) mutateFactor = neuronConfig.getMutateFactorMax();
    }

    private void modifyMutateFactorPositive(boolean wasActive) {
        if (!wasActive) return;
        mutateFactor -= neuronConfig.getMutateFactorMutate() * Utils.random();
        if (mutateFactor < neuronConfig.getMutateFactorMin()) mutateFactor = neuronConfig.getMutateFactorMin();
    }

    private void modifyForReact(float educateFactor, boolean wasActive, long iterate) {
        if (!wasActive) {
            makeMoreActiveThreshold(educateFactor);
        }
        powersForReact(educateFactor, iterate);
    }

    private void modifyForNoReact(float educateFactor, boolean wasActive, long iterate) {
        if (wasActive) {
            makeLessActiveThreshold(educateFactor);
            decreasePower(educateFactor, iterate);
        }
    }

    private float powerValueIncrementer(float educateFactor) {
        float v = neuronConfig.getPowerMutate() * mutateFactor * educateFactor * Utils.random();
        if (v < 0) throw new WrongNeuronState("power value incrementer negative ef:" + educateFactor);
        return v;
    }

//    private void powersForReactOld(float educateFactor, long iterationForLearn) {
//        if (nextNeuronsWithPower == null || nextNeuronsWithPower.isEmpty()) return;
//        for (NeuronKey key : nextNeuronsWithPower.keySet()) {
//            Float power = nextNeuronsWithPower.get(key);
//            Neuron nextNeuron = neuronConfig.getAllNeurons().get(key);
//            if (!nextNeuron.wasActive(iterationForLearn)) {
//                power += powerValueIncrementer(educateFactor);
//                power = normalizePower(power);
//                nextNeuronsWithPower.put(key, power);
//            }
//        }
//    }

    private void powersForReact(float educateFactor, long iterationForLearn) {
        if (synapses == null || synapses.isEmpty()) return;
        if (wasActive(iterationForLearn)) return;
        for (NeuronKey key : synapses.keySet()) {
            Float power = synapses.get(key);
            power += powerValueIncrementer(educateFactor);
            power = normalizePower(power);
            synapses.put(key, power);
        }
    }

//    private void decreasePowerOld(float educateFactor, long iterationForLearn) {
//        if (nextNeuronsWithPower == null || nextNeuronsWithPower.isEmpty()) return;
//        for (NeuronKey key : nextNeuronsWithPower.keySet()) {
//            Neuron neuron = neuronConfig.getAllNeurons().get(key);
//            if (neuron.wasActive(iterationForLearn)) {
//                Float power = nextNeuronsWithPower.get(key);
//                power -= powerValueIncrementer(educateFactor);
//                power = normalizePower(power);
//                nextNeuronsWithPower.put(key, power);
//            }
//        }
//    }

    private void decreasePower(float educateFactor, long iterationForLearn) {
        if (synapses == null || synapses.isEmpty()) return;
        if (!wasActive(iterationForLearn)) return;
        for (NeuronKey key : synapses.keySet()) {
            Float power = synapses.get(key);
            power -= powerValueIncrementer(educateFactor);
            power = normalizePower(power);
            synapses.put(key, power);

        }
    }

    private float normalizePower(float power) {
        float maxPower = neuronConfig.getMaxPower();
        float minPower = neuronConfig.getMinPower();

        if (inhibitor) {
//            maxPower = 0;
        } else {
//            minPower = 0;
        }
        if (power > maxPower) power = maxPower;
        if (power < minPower) power = minPower;
        return power;
    }

    private void increaseThreshold(float educateFactor) {
        if (!neuronConfig.isMutateThresholdEnable()) return;
        threshold += (neuronConfig.getThresholdMutate() * mutateFactor * educateFactor * Utils.random());
        if (threshold > neuronConfig.getMaxThreshold()) threshold = neuronConfig.getMaxThreshold();
    }

    private void decreaseThreshold(float educateFactor) {
        if (!neuronConfig.isMutateThresholdEnable()) return;
        threshold -= (neuronConfig.getThresholdMutate() * mutateFactor * educateFactor * Utils.random());
        if (threshold < neuronConfig.getMinThreshold()) threshold = neuronConfig.getMinThreshold();
    }

    private void makeMoreActiveThreshold(float educateFactor) {
        if (!inhibitor) {
            if (initiator) {
                decreaseThreshold(educateFactor);
            } else {
                increaseThreshold(educateFactor);
            }
        } else {
            if (initiator) {
                increaseThreshold(educateFactor);
            } else {
                decreaseThreshold(educateFactor);
            }
        }
    }

    private void makeLessActiveThreshold(float educateFactor) {
        if (!inhibitor) {
            if (initiator) {
                increaseThreshold(educateFactor);
            } else {
                decreaseThreshold(educateFactor);
            }
        } else {
            if (initiator) {
                decreaseThreshold(educateFactor);
            } else {
                increaseThreshold(educateFactor);
            }
        }
    }

    public void rememberDecision(long iterate) {
        Decision decision = new Decision(iterate, activate);
        decisions.put(iterate, decision);
    }


    public void addNextNeuron(Neuron neuron) {
        if(neuronConfig == null)
            throw new WrongNeuronState("Config not initialized for " + toString());
        float p;
        if (neuronConfig.isZeroLayerPowerSimple() && key.getLayer() == 0) {
            p = Utils.randomSimpleValues(neuron.getSynapses().values());
        } else {
            p = Utils.randomWithMinus();
        }
        nextNeurons.add(neuron.getKey());

        neuron.getSynapses().put(key, p);
    }


    public void copyValues(Neuron neuron) {
        if (!key.equals(neuron.getKey()))
            throw new NeuronException("Key not equal " +
                    "\n" + this.toString() +
                    "\n" + neuron.toString());
        mutateFactor = neuron.getMutateFactor();
        threshold = neuron.getThreshold();
        nextNeurons = neuron.getNextNeurons();
        synapses = neuron.getSynapses();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Neuron)) return false;

        Neuron neuron = (Neuron) o;

        return getKey() != null ? getKey().equals(neuron.getKey()) : neuron.getKey() == null;
    }

    @Override
    public int hashCode() {
        return getKey() != null ? getKey().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Neuron{" + key.toString() + '}';
    }
}
