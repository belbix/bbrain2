package pro.belbix.bbrain2.neuron;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pro.belbix.bbrain2.exceptions.WrongNeuronState;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
public class NeuronKey implements Comparable<NeuronKey> {
    private final long id;
    private final long layer;
    private final boolean inhibitor;
    private final boolean reverse; //reverse neuron activate when signal decrease
    private final boolean initiator;

    @Override
    public String toString() {
        String inhibitor;
        String reverse;
        String initiator;
        if (this.inhibitor) {
            inhibitor = "inh";
        } else {
            inhibitor = "act";
        }
        if (this.reverse) {
            reverse = "rev";
        } else {
            reverse = "dir";
        }
        if (this.initiator) {
            initiator = "ini";
        } else {
            initiator = "fin";
        }
        return layer + ":" + id + ":" + inhibitor + ":" + reverse + ":" + initiator;
    }

    @Override
    public int compareTo(NeuronKey o) {
        if (o.equals(this)) return 0;
        if (o.getLayer() > layer) return -1;
        if (o.getLayer() < layer) return 1;
        if (o.getId() > id) return -1;
        if (o.getId() < id) return 1;

        if (o.isInitiator() && !initiator) return 1;
        if (!o.isInitiator() && initiator) return -1;
        if (o.isInhibitor() && !inhibitor) return -1;
        if (!o.isInhibitor() && inhibitor) return 1;
        if (o.isReverse() && !reverse) return -1;
        if (!o.isReverse() && reverse) return 1;

        throw new WrongNeuronState("Neuron keys o:" + o + " and " + this.toString());
    }
}
