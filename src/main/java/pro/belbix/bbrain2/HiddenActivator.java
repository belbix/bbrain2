package pro.belbix.bbrain2;

import pro.belbix.bbrain2.neuron.Neuron;
import pro.belbix.bbrain2.neuron.NeuronCalculator;

import java.util.List;
import java.util.concurrent.CountDownLatch;

public class HiddenActivator implements Activator {
    private CountDownLatch latch;
    private final List<Neuron> neurons;
    private final Boolean positive;

    public HiddenActivator(List<Neuron> neurons, Boolean positive) {
        this.neurons = neurons;
        this.positive = positive;
    }

    @Override
    public void run() {
        for(Neuron neuron: neurons){
            NeuronCalculator.activateNeuron(neuron, positive);
        }

        latch.countDown();
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }
}
