package pro.belbix.bbrain2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Utils {

    public static float random() {
        return (float) Math.random();
    }

    public static float randomWithMinus() {
        if (Math.random() > 0.5) {
            return (float) Math.random();
        } else {
            return (float) -Math.random();
        }
    }

    public static float randomSimpleValues(Collection<Float> existPowers) {
        if (existPowers.size() < 1) {
            if (Math.random() > 0.5) {
                return 1;
            } else {
                return -1;
            }
        }

        boolean positiveExist = false;
        boolean negativeExist = false;
        for (Float p : existPowers) {
            if (p > 0) {
                positiveExist = true;
            } else if (p < 0) {
                negativeExist = true;
            }
        }

        if (!positiveExist) {
            return 1;
        }
        if (!negativeExist) {
            return -1;
        }

        if (existPowers.size() == 2) return 1;

        if (Math.random() > 0.5) {
            return 1;
        } else {
            return -1;
        }
    }

    public static List<Float> normalizeByte(List<Byte> bytes){
        List<Float> data = new ArrayList<>();
        for(Byte aByte: bytes){
            Float f = aByte.floatValue() / 255;
            data.add(f);
        }
        return data;
    }

}
