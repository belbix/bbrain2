package pro.belbix.bbrain2.neuron2;

import java.util.List;
import java.util.Map;

public class NetworkArrays {

    private Map<Long, float[]> receptorThresholds; //<layerId, values matrix>
    private Map<Long, float[]> receptorMutates; //<layerId, values matrix>
    private Map<Long, float[][]> receptorNexteurons; //<layerId, values matrix>

}
