package pro.belbix.bbrain2;

import org.lwjgl.Sys;
import pro.belbix.bbrain2.exceptions.DuplicateNeuron;
import pro.belbix.bbrain2.exceptions.NeuronNotFound;
import pro.belbix.bbrain2.neuron.Network;
import pro.belbix.bbrain2.neuron.Neuron;
import pro.belbix.bbrain2.neuron.NeuronConfig;
import pro.belbix.bbrain2.neuron.NeuronKey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class LayerCreator {

    public static Network createMultiLayers(long receptorCount, long hiddenCount, long hiddenLayersCount, long resultCount, NeuronConfig neuronConfig) {
        Map<NeuronKey, Neuron> allNeurons = new TreeMap<>();
        Network network = new Network();
        network.setNeuronConfig(neuronConfig);

        createReceptorLayers(network, receptorCount, neuronConfig, allNeurons);
        long resultLayerId = 0;
        double count = 0;
        for (long hiddenLayerId = 1; hiddenLayerId <= hiddenLayersCount; hiddenLayerId++) {
            long hiddenCountR = hiddenCount - (long) (((double) hiddenCount / (double) hiddenLayersCount) * count);
            createHiddenLayers(network, hiddenCountR, neuronConfig, allNeurons, hiddenLayerId);
            resultLayerId = hiddenLayerId + 1;
            count++;
        }
        createResultLayer(network, resultCount, neuronConfig, allNeurons, resultLayerId);

        neuronConfig.setAllNeurons(allNeurons);
        for (Neuron neuron : allNeurons.values()) {
            neuron.setNeuronConfig(neuronConfig);
        }

        connectReceptorToHidden(network);
        connectHiddenToHidden(network, hiddenLayersCount);
        connectHiddenToResult(network, hiddenLayersCount);

        return network;
    }

    public static Network createLayers(long receptorCount, long hiddenCount, long resultCount, NeuronConfig neuronConfig) {
        Map<NeuronKey, Neuron> allNeurons = new TreeMap<>();
        Network network = new Network();
        network.setNeuronConfig(neuronConfig);

        createReceptorLayers(network, receptorCount, neuronConfig, allNeurons);
        createHiddenLayers(network, hiddenCount, neuronConfig, allNeurons, 1);
        createResultLayer(network, resultCount, neuronConfig, allNeurons, 2);

        neuronConfig.setAllNeurons(allNeurons);
        for (Neuron neuron : allNeurons.values()) {
            neuron.setNeuronConfig(neuronConfig);
        }

        connectReceptorToHidden(network);
        connectHiddenToResult(network, 1);

        return network;
    }

    private static Map<Long, Neuron> createLayer(long count, NeuronKey key, Map<NeuronKey, Neuron> allNeurons) {
        long layerId = key.getLayer();
        boolean inhibitor = key.isInhibitor();
        boolean reverse = key.isReverse();
        boolean initiator = key.isInitiator();

        Map<Long, Neuron> neurons = new TreeMap<>();
        for (long neuronId = 0; neuronId < count; neuronId++) {
            Neuron neuron = new Neuron(neuronId, layerId, inhibitor, reverse, initiator);
            neurons.put(neuronId, neuron);
            if (allNeurons.containsKey(neuron.getKey()))
                throw new DuplicateNeuron("All neurons collection already have " + neuron.getKey());
            allNeurons.put(neuron.getKey(), neuron);
        }
        return neurons;
    }

    private static void connectReceptorToHidden(Network network) {
        for (Map<Long, Neuron> layer : network.getReceptorLayers()) {
            NeuronKey key = layer.values().iterator().next().getKey();

            List<Map<Long, Neuron>> hLayers = network.getHiddenLayersByKey(key, 1);
            for (Map<Long, Neuron> hLayer : hLayers) {
                connectLayers(layer, hLayer, network.getNeuronConfig());
            }
        }
    }

    private static void connectHiddenToHidden(Network network, long hiddenLayersCount) {
        Map<Long, Map<Long, Neuron>> layers = new TreeMap<>();
        for (Map<Long, Neuron> layer : network.getHiddenLayers()) {
            NeuronKey key = layer.values().iterator().next().getKey();
            layers.put(key.getLayer(), layer);
        }
        for (long hiddenLayerId = 1; hiddenLayerId <= hiddenLayersCount; hiddenLayerId++) {
            Map<Long, Neuron> hLayer1 = layers.get(hiddenLayerId);
            Map<Long, Neuron> hLayer2 = layers.get(hiddenLayerId + 1);
            if (hLayer2 == null) break;
            connectLayers(hLayer1, hLayer2, network.getNeuronConfig());
        }
    }

    private static void connectLayers(Map<Long, Neuron> firstLayer, Map<Long, Neuron> secondLayer, NeuronConfig neuronConfig) {

        long r = neuronConfig.getRadiusForConnect();
//        long delta = secondLayer.size() - firstLayer.size();
//        if (r > delta) r = delta;
        for (long i : firstLayer.keySet()) {
            for (long j = -r; j <= r; j++) {
                if (j < 0 || j > secondLayer.size() - 1) continue;
                Neuron neuron = firstLayer.get(i);
                Neuron sN = secondLayer.get(j);
                if (sN == null) {
                    throw new NeuronNotFound("Not found neuron for " + neuron + " j:" + j);
                }
                neuron.addNextNeuron(sN);
            }
        }
    }

    private static void connectHiddenToResult(Network network, long lastHiddenLayerId) {
        for (Map<Long, Neuron> layer : network.getHiddenLayers()) {
            NeuronKey hKey = layer.values().iterator().next().getKey();
            if (hKey.getLayer() != lastHiddenLayerId) continue;
            Neuron rNeuron = network.getResultLayerByKey(hKey);

            for (Neuron hNeuron : layer.values()) {
                hNeuron.addNextNeuron(rNeuron);
            }
        }
    }

    private static void createReceptorLayers(Network network, long receptorCount, NeuronConfig neuronConfig, Map<NeuronKey, Neuron> allNeurons) {
        List<Map<Long, Neuron>> receptorLayers = new ArrayList<>();

        receptorLayers.add(createLayer(receptorCount, new NeuronKey(0, 0, false, false, true), allNeurons));
        if (neuronConfig.isUseFinisher()) {
            receptorLayers.add(createLayer(receptorCount, new NeuronKey(0, 0, false, false, false), allNeurons));
        }
        if (neuronConfig.isUseReverse()) {
            if (neuronConfig.isUseFinisher()) {
                receptorLayers.add(createLayer(receptorCount, new NeuronKey(0, 0, false, true, false), allNeurons));
            }
            receptorLayers.add(createLayer(receptorCount, new NeuronKey(0, 0, false, true, true), allNeurons));
        }

        if (neuronConfig.isUseInhibitor()) {
            if (neuronConfig.isUseFinisher()) {
                receptorLayers.add(createLayer(receptorCount, new NeuronKey(0, 0, true, false, false), allNeurons));
            }
            receptorLayers.add(createLayer(receptorCount, new NeuronKey(0, 0, true, false, true), allNeurons));
            if (neuronConfig.isUseReverse()) {
                receptorLayers.add(createLayer(receptorCount, new NeuronKey(0, 0, true, true, true), allNeurons));
                if (neuronConfig.isUseFinisher()) {
                    receptorLayers.add(createLayer(receptorCount, new NeuronKey(0, 0, true, true, false), allNeurons));
                }
            }
        }
        network.setReceptorLayers(receptorLayers);
    }

    private static void createHiddenLayers(Network network, long hiddenCount, NeuronConfig neuronConfig, Map<NeuronKey, Neuron> allNeurons, long layerId) {
        List<Map<Long, Neuron>> hiddenLayers = new ArrayList<>();
        hiddenLayers.add(createLayer(hiddenCount, new NeuronKey(0, layerId, false, false, true), allNeurons));
        if (neuronConfig.isUseFinisher()) {
            hiddenLayers.add(createLayer(hiddenCount, new NeuronKey(0, layerId, false, false, false), allNeurons));
        }
        if (neuronConfig.isUseReverse()) {
            hiddenLayers.add(createLayer(hiddenCount, new NeuronKey(0, layerId, false, true, true), allNeurons));
            if (neuronConfig.isUseFinisher()) {
                hiddenLayers.add(createLayer(hiddenCount, new NeuronKey(0, layerId, false, true, false), allNeurons));
            }
        }
        if (neuronConfig.isUseInhibitor()) {
            hiddenLayers.add(createLayer(hiddenCount, new NeuronKey(0, layerId, true, false, true), allNeurons));
            if (neuronConfig.isUseFinisher()) {
                hiddenLayers.add(createLayer(hiddenCount, new NeuronKey(0, layerId, true, false, false), allNeurons));
            }
            if (neuronConfig.isUseReverse()) {
                hiddenLayers.add(createLayer(hiddenCount, new NeuronKey(0, layerId, true, true, true), allNeurons));
                if (neuronConfig.isUseFinisher()) {
                    hiddenLayers.add(createLayer(hiddenCount, new NeuronKey(0, layerId, true, true, false), allNeurons));
                }
            }
        }
        if (network.getHiddenLayers() == null) {
            network.setHiddenLayers(hiddenLayers);
        } else {
            network.getHiddenLayers().addAll(hiddenLayers);
        }
    }

    private static void createResultLayer(Network network, long resultCount, NeuronConfig neuronConfig, Map<NeuronKey, Neuron> allNeurons, long layerId) {
        List<Neuron> results = new ArrayList<>(createLayer(resultCount, new NeuronKey(0, layerId, false, false, true), allNeurons).values());
        if (neuronConfig.isUseFinisher()) {
            results.addAll(createLayer(resultCount, new NeuronKey(0, layerId, false, false, false), allNeurons).values());
        }
        if (neuronConfig.isUseReverse()) {
            results.addAll(createLayer(resultCount, new NeuronKey(0, layerId, false, true, true), allNeurons).values());
            if (neuronConfig.isUseFinisher()) {
                results.addAll(createLayer(resultCount, new NeuronKey(0, layerId, false, true, false), allNeurons).values());
            }
        }
        network.setResults(results);
    }


}
