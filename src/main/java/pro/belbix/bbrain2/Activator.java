package pro.belbix.bbrain2;

import java.util.concurrent.CountDownLatch;

public interface Activator extends Runnable{
    void setLatch(CountDownLatch latch);
}
