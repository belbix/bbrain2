package pro.belbix.bbrain2.exceptions;

public class NeuronNotFound extends NeuronException {
    public NeuronNotFound(String message) {
        super(message);
    }
}
