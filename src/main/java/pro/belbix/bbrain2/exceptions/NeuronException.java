package pro.belbix.bbrain2.exceptions;

public class NeuronException extends RuntimeException {
    public NeuronException() {
    }

    public NeuronException(String message) {
        super(message);
    }

    public NeuronException(String message, Throwable cause) {
        super(message, cause);
    }

    public NeuronException(Throwable cause) {
        super(cause);
    }

    public NeuronException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
