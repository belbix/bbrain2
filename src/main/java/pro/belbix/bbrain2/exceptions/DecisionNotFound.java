package pro.belbix.bbrain2.exceptions;

public class DecisionNotFound extends NeuronException {
    public DecisionNotFound(String message) {
        super(message);
    }
}
