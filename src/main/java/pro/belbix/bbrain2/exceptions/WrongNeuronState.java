package pro.belbix.bbrain2.exceptions;

public class WrongNeuronState extends NeuronException {
    public WrongNeuronState(String message) {
        super(message);
    }
}
