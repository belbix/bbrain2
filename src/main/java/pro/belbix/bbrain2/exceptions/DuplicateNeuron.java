package pro.belbix.bbrain2.exceptions;

public class DuplicateNeuron extends NeuronException {
    public DuplicateNeuron(String message) {
        super(message);
    }
}
