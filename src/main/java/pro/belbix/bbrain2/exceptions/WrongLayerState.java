package pro.belbix.bbrain2.exceptions;

public class WrongLayerState extends NeuronException {
    public WrongLayerState(String message) {
        super(message);
    }
}
