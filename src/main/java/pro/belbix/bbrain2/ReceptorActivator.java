package pro.belbix.bbrain2;

import pro.belbix.bbrain2.exceptions.WrongNeuronState;
import pro.belbix.bbrain2.neuron.Neuron;
import pro.belbix.bbrain2.neuron.NeuronCalculator;

import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class ReceptorActivator implements Activator {
    private CountDownLatch latch;
    private final Map<Neuron, Float> mappedValues;
    private final Boolean positive;

    public ReceptorActivator(Map<Neuron, Float> mappedValues, Boolean positive) {
        this.mappedValues = mappedValues;
        this.positive = positive;
    }

    @Override
    public void run() {
        for(Neuron neuron: mappedValues.keySet()){
            Float d = mappedValues.get(neuron);
            if(d == null) throw new WrongNeuronState("data is null for " + neuron);
            neuron.plusValue(d);
            NeuronCalculator.activateNeuron(neuron, positive);
        }
        latch.countDown();
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }
}
