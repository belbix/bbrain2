package pro.belbix.bbrain2;

import pro.belbix.bbrain2.neuron.Network;
import pro.belbix.bbrain2.neuron.Neuron;
import pro.belbix.bbrain2.neuron.NeuronCalculator;
import pro.belbix.bbrain2.neuron.NeuronKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class ProcessingCenter {
    private static final int RECEPTOR_THREAD_COUNT = 100;
    private static final int HIDDEN_THREAD_COUNT = 100;

    public static void handleData(List<Float> data, Network network, Boolean positive) {
        handleReceptors(data, network, positive);
        handleDataWithoutReceptors(network, positive);
    }

    public static void handleDataWithoutReceptors(Network network, Boolean positive) {
        handleHidden(network);

        for (Neuron n : network.getResults()) {
            NeuronCalculator.activateNeuron(n, positive);
        }
    }

    public static void handleDataOrdered(List<Float> data, Network network) {
        handleReceptors(data, network, null);

        for (Map<Long, Neuron> layer : network.getHiddenLayers()) {
            handleHiddenLayer(layer, false);
            handleHiddenLayer(layer, true);
        }

        for (Neuron n : network.getResults()) {
            NeuronCalculator.activateNeuron(n, false);
            NeuronCalculator.activateNeuron(n, true);
        }


    }

    private static void handleReceptors(List<Float> data, Network network, Boolean positive) {
        Map<Neuron, Float> mappedValuesForReceptor = new HashMap<>();
        List<Activator> receptorActivators = new ArrayList<>();
        long count = 0;
        long allCount = (data.size() * network.getReceptorLayers().size());
        long k = Math.round((double) allCount / (double) RECEPTOR_THREAD_COUNT);
        boolean useActivators = true;
        if (k < 10) useActivators = false;
        for (Map<Long, Neuron> layer : network.getReceptorLayers()) {
            for (long i = 0; i < data.size(); i++) {
                Neuron n = layer.get(i);
                Float d = data.get((int) i);
                if (useActivators) {
                    mappedValuesForReceptor.put(n, d);
                    if (count % k == 0) {
                        ReceptorActivator receptorActivator = new ReceptorActivator(new HashMap<>(mappedValuesForReceptor), positive);
                        receptorActivators.add(receptorActivator);
                        mappedValuesForReceptor.clear();
                    }
                } else {
                    n.plusValue(d); //TODO delete if want use ordering in receptors
                    NeuronCalculator.activateNeuron(n, positive);
                }
                count++;
            }
        }
        activatorExecutor(receptorActivators);
    }

    private static void handleHidden(Network network) {
        for (Map<Long, Neuron> layer : network.getHiddenLayers()) {
            handleHiddenLayer(layer, null);
        }

    }

    private static void handleHiddenLayer(Map<Long, Neuron> layer, Boolean positive) {
        List<Activator> hiddenActivators = new ArrayList<>();
        List<Neuron> neurons = new ArrayList<>();
        boolean useActivator = true;
        long count = 0;
        long k = Math.round((double) layer.values().size() / (double) HIDDEN_THREAD_COUNT);
        if (k < 10) useActivator = false;
        for (Neuron n : layer.values()) {
            if (useActivator) {
                neurons.add(n);
                if (count % k == 0) {
                    hiddenActivators.add(new HiddenActivator(new ArrayList<>(neurons), positive));
                    neurons.clear();
                }
            } else {
                NeuronCalculator.activateNeuron(n, positive);
            }
            count++;
        }
        activatorExecutor(hiddenActivators);
    }



    private static void activatorExecutor(List<Activator> receptorActivators){
        CountDownLatch latch = new CountDownLatch(receptorActivators.size());
        for(Activator activator: receptorActivators){
            activator.setLatch(latch);
            new Thread(activator).start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void rememberAndFinishing(Map<NeuronKey, Neuron> allNeurons, long iteration) {
        for (Neuron neuron : allNeurons.values()) {
            neuron.rememberDecision(iteration);
            neuron.iterateFinish();
        }
    }

    public static void educateNegative(Map<NeuronKey, Neuron> allNeurons, float educateFactor, long iteration, boolean needToReact) {
        educateFactor = Math.abs(educateFactor);
        for (Neuron neuron : allNeurons.values()) {
            neuron.educateNegative(educateFactor, needToReact, iteration);
        }
    }

    public static void educatePositive(Map<NeuronKey, Neuron> allNeurons, float educateFactor, long iteration) {
        educateFactor = Math.abs(educateFactor);
        for (Neuron neuron : allNeurons.values()) {
            neuron.educatePositive(educateFactor, iteration);
        }
    }

    public static void educateLazy(Map<NeuronKey, Neuron> allNeurons, float educateFactor, long iteration) {
        for (Neuron neuron : allNeurons.values()) {
            neuron.educateLazy(educateFactor, iteration);
        }
    }

    public static void clear(Map<NeuronKey, Neuron> allNeurons) {
        for (Neuron neuron : allNeurons.values()) {
            neuron.clear();
        }
    }

    public static void clearNetworks(Map<Long, Network> networks) {
        for (Network network : networks.values()) {
            clear(network.getNeuronConfig().getAllNeurons());
        }
    }

}
