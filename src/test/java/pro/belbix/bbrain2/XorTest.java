package pro.belbix.bbrain2;

import lombok.AllArgsConstructor;
import org.junit.Assert;
import org.junit.Test;
import pro.belbix.bbrain2.neuron.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class XorTest {
    private static final int ITERATIONS = 100000;
    private static final int COUNT_OF_LOG = 10;

    private static final int EDUCATE_FACTOR = 10;

    @Test
    public void multipleXor() {
        for (int i = 0; i < 1000; i++) {
            xor();
            System.out.println("End cycle xor " + i);
        }
    }


    @Test
    public void xor() {
        NeuronConfig neuronConfig = new NeuronConfig();
        neuronConfig.setUseInhibitor(false);
        neuronConfig.setUseReverse(false);
        neuronConfig.setUseFinisher(false);
        Network network = LayerCreator.createMultiLayers(2, 3, 5, 1, neuronConfig);

//        setupStaticLayerValue(network);

        NeuronKey key = new NeuronKey(0, 0, false, false, true);
        List<Value> values = createData();


        long iteration = 0;
        for (int i = 0; i < ITERATIONS; i++) {
            double allDecision = 0d;
            double goodDecision = 0d;
            for (Value value : values) {
                List<Float> data = new ArrayList<>();
                data.add(value.a);
                data.add(value.b);
                ProcessingCenter.handleDataOrdered(data, network);

                boolean result = network.result(key);

                allDecision++;
                boolean goodResult = result == value.react;
                if (goodResult) goodDecision++;

                ProcessingCenter.rememberAndFinishing(neuronConfig.getAllNeurons(), iteration);

                if (!goodResult) {
                    ProcessingCenter.educateNegative(neuronConfig.getAllNeurons(), EDUCATE_FACTOR, iteration, value.react);
                } else {
                    ProcessingCenter.educatePositive(neuronConfig.getAllNeurons(), EDUCATE_FACTOR, iteration);
                }

                iteration++;
            }
            double rate = ((goodDecision / allDecision) * 100);
            if (i % (ITERATIONS / COUNT_OF_LOG) == 0) {
                System.out.println("Iterate rate:" + rate);
                if (i > (ITERATIONS * 0.85) && rate < 100) {
                    System.out.println(Printer.printNetwork(network));
//                    System.exit(-1);
                }
            }
            if (i == ITERATIONS - 1) {
                Assert.assertEquals(100, rate, 0);
            }
        }


    }

    private void setupStaticLayerValue(Network network) {
        for (Map<Long, Neuron> hLayer : network.getHiddenLayers()) {
            Neuron nHidden0 = hLayer.get(0L);
            nHidden0.setThreshold(0.7106633f);
            for (NeuronKey key : nHidden0.getSynapses().keySet()) {
                if (key.getId() == 0) {
                    nHidden0.getSynapses().put(key, 8.559721f);
                } else if (key.getId() == 1) {
                    nHidden0.getSynapses().put(key, -7.842237f);
                }
            }

            Neuron nHidden1 = hLayer.get(1L);
            nHidden1.setThreshold(0.8078166f);
            for (NeuronKey key : nHidden1.getSynapses().keySet()) {
                if (key.getId() == 0) {
                    nHidden1.getSynapses().put(key, 8.525187f);
                } else if (key.getId() == 1) {
                    nHidden1.getSynapses().put(key, -7.716875f);
                }
            }

            Neuron nHidden2 = hLayer.get(2L);
            nHidden2.setThreshold(0.43731174f);
            for (NeuronKey key : nHidden2.getSynapses().keySet()) {
                if (key.getId() == 0) {
                    nHidden2.getSynapses().put(key, 6.2698946f);
                } else if (key.getId() == 1) {
                    nHidden2.getSynapses().put(key, -5.741873f);
                }
            }
        }

        Neuron rNeuron = network.getResults().iterator().next();
        rNeuron.setThreshold(0.43822327f);
        Map<NeuronKey, Float> rSynapses = rNeuron.getSynapses();
        for (NeuronKey key : rSynapses.keySet()) {
            if (key.getId() == 0) {
                rSynapses.put(key, 0.7745215f);
            } else if (key.getId() == 1) {
                rSynapses.put(key, 2.9236357f);
            } else if (key.getId() == 2) {
                rSynapses.put(key, -2.4982157f);
            }
        }


    }

    private List<Value> createData() {
        List<Value> values = new ArrayList<>();
        values.add(new Value(0, 0, false));
        values.add(new Value(1, 0, true));
        values.add(new Value(0, 1, true));
        values.add(new Value(1, 1, false));
        return values;
    }

    @AllArgsConstructor
    class Value {
        float a;
        float b;
        boolean react;
    }

}
