package pro.belbix.bbrain2;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import pro.belbix.bbrain2.neuron.Network;
import pro.belbix.bbrain2.neuron.Neuron;
import pro.belbix.bbrain2.neuron.NeuronConfig;
import pro.belbix.bbrain2.neuron.NeuronKey;

import java.io.File;
import java.io.FileInputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class CifarTest2 {
    private static final int ITERATIONS = 100000;
    private static final int COUNT_OF_LOG = 100;
    private static final int EDUCATE_FACTOR = 1;

    private static final int RECEPTOR_COUNT = 3072;
    private static final double HIDDEN_COUNT_FACTOR = 3;

    private Map<Long, Network> networks() {
        Map<Long, Network> map = new HashMap<>();
        for (long i = 0; i < 10; i++) {
            NeuronConfig neuronConfig = new NeuronConfig();
            neuronConfig.setUseInhibitor(false);
            neuronConfig.setUseReverse(false);
            neuronConfig.setUseFinisher(false);
            map.put(i, LayerCreator.createMultiLayers(RECEPTOR_COUNT, (long) ((double) RECEPTOR_COUNT * HIDDEN_COUNT_FACTOR), 5, 1, neuronConfig));
        }
        return map;
    }

    private static long calcIteration(Value value, Map<Long, Network> networks, long iteration, boolean educate) {
        NeuronKey key = new NeuronKey(0, 0, false, false, true);
        long resultGroup = -1;
        boolean makeDecision = false;
        for (Long netKey : networks.keySet()) {
            Network network = networks.get(netKey);
            Map<NeuronKey, Neuron> allNeurons = network.getNeuronConfig().getAllNeurons();

//            Instant start = Instant.now();
            ProcessingCenter.handleData(value.data, network, null);
//            System.out.println("handleData " + Duration.between(start, Instant.now()).toMillis());

            boolean results = network.result(key);
            if (results) {
                if (!makeDecision) {
                    makeDecision = true;
                    resultGroup = netKey;
                } else {
                    resultGroup = -1;
                }
            }

            boolean goodResult;
            boolean needToReact;

            if (value.group == netKey) {
                needToReact = true;
                goodResult = results;
            } else {
                needToReact = false;
                goodResult = !results;
            }

            ProcessingCenter.rememberAndFinishing(allNeurons, iteration);

            if (educate) {
                if (!goodResult) {
                    ProcessingCenter.educateNegative(allNeurons, EDUCATE_FACTOR, iteration, needToReact);
                } else {
                    ProcessingCenter.educatePositive(allNeurons, EDUCATE_FACTOR, iteration);
                }
            }
        }
        return resultGroup;
    }

    private double educate(Map<Long, Network> networks, List<Value> values, boolean educate) {
        double rate = 0d;
        for (int i = 0; i < ITERATIONS; i++) {
            long iteration = 0;
            ProcessingCenter.clearNetworks(networks);
            double goodDecision = 0d;
            Instant startGlobal = Instant.now();
            Instant start = Instant.now();
            for (Value value : values) {
                Instant time = Instant.now();

                long resultGroup = calcIteration(value, networks, iteration, educate);

                if (value.group == resultGroup) goodDecision++;

                Duration educateTime = Duration.between(time, Instant.now());

                iteration++;
                rate = ((goodDecision / (double) iteration) * 100);
                double k = ((double) values.size() / (double) COUNT_OF_LOG);
                if (Math.round(k) != 0 && iteration % Math.round(k) == 0) {
                    long t = Math.round((double) Duration.between(start, Instant.now()).toMillis() / k);
                    System.out.println("Progress: " + Math.round(iteration / k)
                            + " rate:" + rate
                            + " goodDecision:" + goodDecision
                            + " iteration:" + iteration
                            + " middle time: " + t
                            + " values.size(): " + values.size()
                            + " educateTime:" + educateTime.toMillis()
                    );
                    start = Instant.now();
//                if (i > 1000 && rate < 100) {
//                    System.out.println(Printer.printNetwork(networks.values().iterator().next()));
//                    System.exit(-1);
//                }
                }

            }
            if (!educate) break;

        }

        return rate;
    }

    @Test
    public void cifarWithSeparateNetworks() {
        Map<Long, Network> networks = networks();
        List<Value> values1 = load("C:\\trainsets\\cifar-10\\data_batch_1.bin");
        double eduRate = educate(networks, values1, true);
        System.out.println("eduRate: " + eduRate);

        List<Value> valuesForTest = load("C:\\trainsets\\cifar-10\\test_batch.bin");
        double testRate = educate(networks, valuesForTest, false);
        System.out.println("testRate: " + testRate);


    }


    public List<Value> load(String path) {
        List<Value> data = new ArrayList<>();
        File file = new File(path);
        try {
            FileInputStream is = new FileInputStream(file);
            byte[] b = new byte[3073];
            int count = 0;
            while (is.read(b) != -1) {
                if (count >= 100) break;
                List<Byte> bytes = new ArrayList<>(Arrays.asList(ArrayUtils.toObject(b)));
                Byte group = bytes.remove(0);
                data.add(new Value(Utils.normalizeByte(bytes), group));
                count++;
            }
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @AllArgsConstructor
    class Value {
        List<Float> data;
        float group;
    }

}
