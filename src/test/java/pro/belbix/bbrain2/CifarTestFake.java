package pro.belbix.bbrain2;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import pro.belbix.bbrain2.neuron.Network;
import pro.belbix.bbrain2.neuron.NeuronConfig;

import java.io.File;
import java.io.FileInputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class CifarTestFake {
    private static final int ITERATIONS = 100000;
    private static final int COUNT_OF_LOG = 100;
    private static final int EDUCATE_FACTOR = 10;
    private static final int GROUP = 1;
    private static final int RECEPTOR_COUNT = 3072;

    private Map<Long, Network> networks() {
        Map<Long, Network> map = new HashMap<>();
        for (long i = 0; i < 10; i++) {
            NeuronConfig neuronConfig = new NeuronConfig();
//        neuronConfig.setUseInhibitor(false);
            neuronConfig.setUseReverse(false);
            neuronConfig.setUseFinisher(false);
            map.put(i, LayerCreator.createLayers(RECEPTOR_COUNT, (long) (RECEPTOR_COUNT * 1.5), 1, neuronConfig));
        }
        return map;
    }

    @Test
    public void cifarFake() {


        Map<Long, Network> networks = networks();
        List<Value> values1 = load("C:\\tim\\cifar-10\\data_batch_1.bin");
        double eduRate = educate(networks, values1, true);
        System.out.println("eduRate: " + eduRate);

        List<Value> valuesForTest = load("C:\\tim\\cifar-10\\test_batch.bin");
        double testRate = educate(networks, valuesForTest, false);
        System.out.println("testRate: " + testRate);


    }

    private double educate(Map<Long, Network> networks, List<Value> values, boolean educate) {
        double rate = 0d;
        for (int i = 0; i < ITERATIONS; i++) {
            long iteration = 0;
            double goodDecision = 0d;
            Instant start = Instant.now();
            for (Value value : values) {
                Instant time = Instant.now();

                long resultGroup = Math.round(Math.random() * 10);

                if(value.group == resultGroup) goodDecision++;

                Duration educateTime = Duration.between(time, Instant.now());

                iteration++;
                rate = ((goodDecision / (double) iteration) * 100);
                if (iteration % (values.size() / COUNT_OF_LOG) == 0) {
                    double t = ((double) Duration.between(start, Instant.now()).toMillis() / ((double) values.size() / (double) COUNT_OF_LOG));
                    System.out.println("Progress: " + (iteration / (values.size() / COUNT_OF_LOG)) + " rate:" + rate
                            + " middle time: " + t +
                            " educateTime:" + educateTime
                    );
                    start = Instant.now();
//                if (i > 1000 && rate < 100) {
//                    System.out.println(Printer.printNetwork(network));
//                    System.exit(-1);
//                }
                }

            }
            if (!educate) break;

        }

        return rate;
    }


    public List<Value> load(String path) {
        List<Value> data = new ArrayList<>();
        File file = new File(path);
        try {
            FileInputStream is = new FileInputStream(file);
            byte[] b = new byte[3073];
            while (is.read(b) != -1) {
                List<Byte> bytes = new ArrayList<>(Arrays.asList(ArrayUtils.toObject(b)));
                Byte group = bytes.remove(0);
                data.add(new Value(Utils.normalizeByte(bytes), group));
            }
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @AllArgsConstructor
    class Value {
        List<Float> data;
        float group;
    }

}
