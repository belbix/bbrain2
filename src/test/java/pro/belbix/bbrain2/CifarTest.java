package pro.belbix.bbrain2;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;
import pro.belbix.bbrain2.neuron.Network;
import pro.belbix.bbrain2.neuron.NeuronConfig;
import pro.belbix.bbrain2.neuron.NeuronKey;

import java.io.File;
import java.io.FileInputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CifarTest {
    private static final int ITERATIONS = 100000;
    private static final int COUNT_OF_LOG = 100;
    private static final int EDUCATE_FACTOR = 10;
    private static final int GROUP = 1;
    private static final int RECEPTOR_COUNT = 3072;

    @Test
    public void cifar() {
        LayerCreator layerCreator = new LayerCreator();

        NeuronConfig neuronConfig = new NeuronConfig();
//        neuronConfig.setUseInhibitor(false);
        neuronConfig.setUseReverse(false);
        neuronConfig.setUseFinisher(false);

        Network network = layerCreator.createLayers(RECEPTOR_COUNT, RECEPTOR_COUNT + neuronConfig.getRadiusForConnect(), 10, neuronConfig);
        List<Value> values1 = load("C:\\tim\\cifar-10\\data_batch_1.bin");
        double eduRate = educate(network, neuronConfig, values1, true);
        System.out.println("eduRate: " + eduRate);

        List<Value> valuesForTest = load("C:\\tim\\cifar-10\\test_batch.bin");
        double testRate = educate(network, neuronConfig, valuesForTest, false);
        System.out.println("testRate: " + testRate);


    }

    private double educate(Network network, NeuronConfig neuronConfig, List<Value> values, boolean educate) {
        NeuronKey key = new NeuronKey(0, 0, false, false, true);
        double rate = 0d;
        for (int i = 0; i < ITERATIONS; i++) {
            long iteration = 0;
            ProcessingCenter.clear(neuronConfig.getAllNeurons());
            double allDecision = 0d;
            double goodDecision = 0d;
            Instant start = Instant.now();
            for (Value value : values) {

                Instant time = Instant.now();
                ProcessingCenter.handleData(value.data, network, null);
                Duration handleDataTime = Duration.between(time, Instant.now());
                time = Instant.now();

                Map<Long, Boolean> results = network.results(key);

                allDecision++;

                boolean goodResult = true;
                boolean needToReact = true;
                boolean found = false;
                for(Long rGroup: results.keySet()){
                    if(rGroup == value.group && !results.get(rGroup)){
                        goodResult = false;
                        needToReact = true;
                        found = true;
                        break;
                    }
                    if(rGroup != value.group && results.get(rGroup)){
                        goodResult = false;
                        needToReact = false;
                        found = true;
                        break;
                    }
                    if(rGroup == value.group && results.get(rGroup)){
                        found = true;
                    }
                }
                Assert.assertTrue(found);

                if (goodResult) goodDecision++;


                ProcessingCenter.rememberAndFinishing(neuronConfig.getAllNeurons(), iteration);
                Duration rememberAndFinishingTime = Duration.between(time, Instant.now());
                time = Instant.now();

                if (educate) {
                    if (!goodResult) {
                        ProcessingCenter.educateNegative(neuronConfig.getAllNeurons(), EDUCATE_FACTOR, iteration, needToReact);
                    } else {
                        ProcessingCenter.educatePositive(neuronConfig.getAllNeurons(), EDUCATE_FACTOR, iteration);
                    }
                }
                Duration educateTime = Duration.between(time, Instant.now());

                iteration++;
                rate = ((goodDecision / allDecision) * 100);
                if (iteration % (values.size() / COUNT_OF_LOG) == 0) {
                    double t = ((double) Duration.between(start, Instant.now()).toMillis() / ((double) values.size() / (double) COUNT_OF_LOG));
                    System.out.println("Progress: " + (iteration / (values.size() / COUNT_OF_LOG)) + " rate:" + rate
                            + " middle time: " + t +
                            " handleDataTime:" + handleDataTime +
                            " rememberAndFinishingTime:" + rememberAndFinishingTime +
                            " educateTime:" + educateTime
                    );
                    start = Instant.now();
//                if (i > 1000 && rate < 100) {
//                    System.out.println(Printer.printNetwork(network));
//                    System.exit(-1);
//                }
                }
            }
            if (!educate) break;

        }

        return rate;
    }


    public List<Value> load(String path) {
        List<Value> data = new ArrayList<>();
        File file = new File(path);
        try {
            FileInputStream is = new FileInputStream(file);
            byte[] b = new byte[3073];
            while (is.read(b) != -1) {
                List<Byte> bytes = new ArrayList<>(Arrays.asList(ArrayUtils.toObject(b)));
                Byte group = bytes.remove(0);
                data.add(new Value(Utils.normalizeByte(bytes), group));
            }
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @AllArgsConstructor
    class Value {
        List<Float> data;
        float group;
    }

}
