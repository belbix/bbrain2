package pro.belbix.bbrain2.neuron;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class NeuronUtilsTest {

    @Test
    public void clearOld() {
    }

    @Test
    public void calcDynamicValues() {
        Map<Long, Float> map = new HashMap<>();

        map.put(1L, 1F);
        map.put(2L, 1F);
        map.put(3L, 1F);

        long countForLife = 2;
        long iterate = 3;
        float v = NeuronUtils.calcDynamicValues(map, countForLife, iterate);
        Assert.assertEquals(1, v, 0);
    }
}